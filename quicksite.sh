#!/bin/bash

######################################################################################################################
#### quicksite.sh
####
#### This is a simple wraper script to help generate a fully featured DjangoCMS 3.x site for use with ViNDaLU
####

DEFAULT_PROJECTPATH='./'
DEFAULT_SITENAME='mysite'
DEFAULT_PY=$(cd $(dirname ${HOME}/python); pwd)/$(basename ${HOME}/python)


function die ( ) {
   echo $@
   exit 1
}


######################################################################################################################
#### Main

echo << EOF
---------------------------------------------------------------------------
IMPORTANT: As a general rule, and to just make things easier - use
only alphanumerics in the site name and project path.  Spaces and other
punctuation (as well as capitalization) can lead to difficulties
echo later on.
---------------------------------------------------------------------------
EOF

read -p "Where we should store this project?: " -e -i ${DEFAULT_PROJECTPATH} PROJECTPATH
read -p "What is the name of the website?: " -e -i ${DEFAULT_SITENAME} SITENAME
read -p "Where is the PVE (Python Virtual Environment): " -e -i ${DEFAULT_PY} PVE

echo "------ Source versioning setup: (not required, and can be changed later)"
read -p "For source versioning with GIT, what is your user name: " -e -i "someuser" GIT_USER
read -p "What email address do you wish to use with GIT: " -e -i "someone@somewhere.com" GIT_EMAIL

# Activate the PVE
source ${PVE}/bin/activate  || die "ERROR: $PVE could not be activated."

# Get the script dir and thus the prototype location
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SOUPMIX="$( cd -P "$( dirname "$SOURCE" )" && pwd )"




# Create the base with the usual command
djangocms \
    -z yes \
    -t America/Los_Angeles \
    -e yes \
    --permissions yes \
    -l en \
    --django-version stable -v stable \
    --starting-page no \
    -f --bootstrap yes \
    -r $SOUPMIX/project_template/requirements.txt \
    -p $PROJECTPATH $SITENAME \
    -q

rm -f $PROJECTPATH/project.db
cp -rf $SOUPMIX/project_template/.gitignore $PROJECTPATH/
cp -rf $SOUPMIX/project_template/etc $PROJECTPATH/
cp -rf $SOUPMIX/project_template/init_db $PROJECTPATH/
cp -rf $SOUPMIX/project_template/publish $PROJECTPATH/
cp -rf $SOUPMIX/project_template/debug $PROJECTPATH/
cp -rf $SOUPMIX/project_template/Vagrantfile $PROJECTPATH/
cp -rf $SOUPMIX/project_template/vagrant-bootstrap.sh $PROJECTPATH/
cp -rf $SOUPMIX/project_template/requirements.txt $PROJECTPATH/
cp -rf $SOUPMIX/project_template/initial_data.json $PROJECTPATH/
cp -rf $SOUPMIX/project_template/project_name/* $PROJECTPATH/$SITENAME/


# This is a little bit whack - but it is functional and is far easier than the alternatives.  ...For the moment
cat $SOUPMIX/project_template/extrasoup_settings.py >> $PROJECTPATH/$SITENAME/settings.py

# Get all the submodules, intialize the local repo and commit the initial file set to it
cd $PROJECTPATH
git init
git config user.name ${GIT_USER}
git config user.email ${GIT_EMAIL}
git submodule add https://github.com/thomaspark/bootswatch.git $SITENAME/static/bootswatch
git submodule update --init
git add -A .
git commit -m 'Initial commit'

# Initialize the site's database
source ./init_db

echo << EOF

DONE!

---------------------------------------------------------------------------
RUN DEVELOPMENT SERVER: (for debugging and testing)

Simply change directory to $PROJECTPATH, activate your PVE and then run 
the development server to test your new server like so:

  source ${PVE}/bin/activate
  cd $PROJECTPATH
  python manage.py runserver 0.0.0.0:8000

This will start up a test server on port 8000!! Now use a browser and take a 
look at your website (http://localhost:8000/)! It really is this easy.

---------------------------------------------------------------------------
REMOTE REPOSITORY: (for code versioning and bug tracking)

To GIT your project on BitBucket (or similar) remote repository, simply login
to git and then create the project on the service.  Then do something similar
to the following:

   git remote add origin git@bitbucket.org:{OWNER}/${SITENAME}.git
   git push --set-upstream origin master
   
---------------------------------------------------------------------------
PUBLISHING THE SITE: (for public viewing)

To publish the site with Nginx and uWSGI, use the publish script.

EOF   

