#!/bin/bash

# Get the script dir and thus the prototype location
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" 
  # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

function die ( ) {
   echo $@
   exit 1
}



#############################
# OS ENVIRONMENT STUFF

# Check the installation of the basic system modules needed
echo << EOF
There are many system requirements needed to make your Django site run smoothly,
such as font and graphics libraries that need to be supported at the OS level.
EOF

read -p "Would you like to check system libraries and environment? (Y/n)? "  -e -i "y"
[ "$REPLY" == "n" ] ||  (
    ###TODO: it might be worth doing a check for these first?
    # dpkg-query -W -f='${Package}\n' apache2 mysql-server php5 2>/dev/null

    # Upgrade the system to current "safe" distribution level
    sudo apt-get update -y || die "ERROR: System update failed! Check your connection."
    sudo apt-get upgrade || die "ERROR: System upgrade failed! Check your error logs for reasons."

    sudo apt-get install -y \
        build-essential python-setuptools python-dev python-virtualenv git-core mercurial gcc python-pip \
        libtidy-dev libfreetype6-dev libjpeg62 libjpeg62-dev libjpeg8 libxml2-dev libxslt1-dev \
        || die "ERROR: could not install all packages as requested."

    # Add either of the following lines.  The first is for the older Apache aproach, the second is for the newer Nginx/uWSGI aproach
    #sudo apt-get install -y node-less apache2 libapache2-mod-wsgi apache2-threaded-dev || die "ERROR: could not install all Apache packages."
    sudo apt-get install node-less nginx uwsgi-plugin-python uwsgi || die "ERROR: could not install all Nginx and uWSGI packages."

    # Check/Link in the graphics libraries for the specific architecture of this system
    MACHINE_TYPE=`uname -m`
    if [ ${MACHINE_TYPE} == 'x86_64' ]; then
        # For Ubuntu x64:
        if [ ! -f /usr/lib/libjpeg.so ]; then
            sudo ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so /usr/lib/
        fi
        if [ ! -f /usr/lib/libfreetype.so ]; then
            sudo ln -s /usr/lib/x86_64-linux-gnu/libfreetype.so /usr/lib/
        fi
        if [ ! -f /usr/lib/libz.so ]; then
            sudo ln -s /usr/lib/x86_64-linux-gnu/libz.so /usr/lib/
        fi
    else
        # Or for Ubuntu 32bit:
        if [ ! -f /usr/lib/libjpeg.so ]; then
            sudo ln -s /usr/lib/i386-linux-gnu/libjpeg.so /usr/lib/
        fi
        if [ ! -f /usr/lib/libfreetype.so ]; then
            sudo ln -s /usr/lib/i386-linux-gnu/libfreetype.so.6 /usr/lib/
        fi
        if [ ! -f /usr/lib/libz.so ]; then
            sudo ln -s /usr/lib/i386-linux-gnu/libz.so /usr/lib/
        fi
    fi
    # NOW, we can cleanly install PIL
    sudo apt-get install -y python-imaging
    # Alternatively, use PILlow

)

# This is actually optional, but suggested for any med-large sites
read -p "Install MySQL?  (It is not required for small to medium sites) (y/N)? "  -e -i "n"
[ "$REPLY" == "n" ] || (
    sudo apt-get install -y mysql-server mysql-client python-mysqldb libmysqlclient-dev || die "ERROR: MySQL was not installed"
)


#############################
# PYTHON VIRTUAL ENVIRONMENT STUFF
DEFAULT_PY=$(cd $(dirname ${HOME}/python); pwd)/$(basename ${HOME}/python)
read -p "Where should the PVE (Python Virtual Environment) be: " -e -i ${DEFAULT_PY} PVE

DEFAULT_CACHE=${PVE}/pip-cache
read -p "Where should we look for and store the PVE install cache: " -e -i ${DEFAULT_CACHE} CACHE

read -p "Should we (re)install a PVE in $PVE? (Y/n)? "  -e -i "y"
[ "$REPLY" == "n" ] ||  (

    # Create/refresh a PVE
    mkdir -p ${CACHE}
    virtualenv --distribute --no-site-packages ${PVE} || die "ERROR: PVE was not created correctly for some reason."
)

# Activate the PVE
source ${PVE}/bin/activate  || die "ERROR: $PVE could not be activated."

# Add uWSGI
read -p "Install uWSGI? Recommended, if we are going to publish this site. (Y/n)? "  -e -i "y"
[ "$REPLY" == "n" ] ||  (
    pip install --download-cache=${CACHE} --exists-action=i -I uwsgi \
        || die "ERROR: uWSGI was not installed correctly."
)

# Install Requirements.txt
read -p "Install frequently used Python requirements? (Y/n)? "  -e -i "y"
[ "$REPLY" == "n" ] ||  (

    DEFAULT_REQUIREMENTS=$(cd $(dirname ${DIR}/project_template/requirements.txt); pwd)/$(basename ${DIR}/project_template/requirements.txt)
    #DEFAULT_REQUIREMENTS="soupmix/project_template/requirements.txt"
    read -p "Requirements file to install: " -e -i ${DEFAULT_REQUIREMENTS} REQUIREMENTS

    pip install --download-cache=${CACHE} --exists-action=i -I -r ${REQUIREMENTS}
    # or if everything is already cached and you are offline, you could use:
    #pip install --download-cache=${CACHE} --find-links=file://${CACHE} --no-index --index-url=file:///dev/null -r ${REQUIREMENTS}
)

echo "========================================"
echo "====== Please READ === Please READ ====="
echo "========================================"
echo
echo "===INSTALLATION DONE!"
echo "OK. We are all done setting up your DjangoCMS environment!"
echo
echo "NOTE: Dont forget to activate your PVE before creating or working on a Django site. "
echo "To do that, just type the following command at the beginning of every session:"
echo
echo "	source $PVE/bin/activate"
echo
echo "===CREATE SITES!"
echo "To create a new site with a default template, simply run the quicksite.sh script."
echo
echo "===UPDATE A SITE"
echo "Frequently, you will need to 'update' your site after installing a new plugin or changing a configuration setting.  To do that, perform the following:"
echo "	source $PVE/bin/activate"
echo "	cd <site_directory>"
echo "	python manage.py syncdb && python manage.py migrate && python manage.py collectstatic -l --noinput"
echo
echo "===NEXT STEPS:"
echo "- Enable additional modules in the <site_name>/settings.py file - like Django Debug Toolbar!"
echo "Check out http://oddotter.com/soupmix for more info, tips and help."

####TODO: put all maintenance tasks into awsfabric or django command extensions instead of bash
