# Welcome to the Soupmix!

The goals of Soupmix are simple, but perhaps not so small.  We intend to create an easy introduction to the creation of Django-CMS applications while not limiting any of the flexibility of the platform to accommodate new and interesting design needs.  Or, in other words, this collection of scripts and files was created for the simple intention of easing the process of creatiing a Django enabled PVE (Python Virtual Environment), creating a web application within that PVE, and ultimately publishing the app to an AWS hosted Ubuntu server platform.

**First-off, a warning:** _While the core components of this toolset are stable, this toolset itself is under rapid development and is subject to modification frequently as we improve and add features while maintaining both stability and functionality.  Please monitor our posts and updates frequently to stay current._

Please see [The soupmix Wiki](https://bitbucket.org/oddotterco/soupmix/wiki/) for more details, instructions and more.

