/**
 * Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

///////////
// This file was copied from the default styles.js in the djangocms_text_ckeditor
// distro and then modified specifically to add support for BootStrap themes
// -- *G* 20140502 djangocms@oddotter.com

CKEDITOR.stylesSet.add( 'default', [
	/* Block Styles */
    // Applied to text blocks (paragraphs) as a whole, not limited to text
    // selections. These apply to the following elements: address, div, h1,
    // h2, h3, h4, h5, h6, p, and pre.

	{ name: 'Italic Title',		element: 'h2', styles: { 'font-style': 'italic' } },
	{ name: 'Subtitle',			element: 'h3', styles: { 'color': '#aaa', 'font-style': 'italic' } },
	{ name: 'Special Container',
		element: 'div',
		styles: {
			padding: '5px 10px',
			background: '#eee',
			border: '1px solid #ccc'
		}
	},
	{ name: 'Block quote',	element: 'blockquote', },

	/* Inline Styles */

	{ name: 'Marker',			element: 'span', attributes: { 'class': 'marker' } },
	{ name: 'Label',			element: 'span', attributes: { 'class': 'label' } },
	{ name: 'Label - primary',	element: 'span', attributes: { 'class': 'primary' } },

	{ name: 'Big',				element: 'big' },
	{ name: 'Small',			element: 'small' },
	{ name: 'Typewriter',		element: 'tt' },

	{ name: 'Computer Code',	element: 'code' },
	//{ name: 'Keyboard Phrase',	element: 'kbd' },
	{ name: 'Sample Text',		element: 'samp' },
	{ name: 'Variable',			element: 'var' },

	{ name: 'Deleted Text',		element: 'del' },
	{ name: 'Inserted Text',	element: 'ins' },

	{ name: 'Cited Work',		element: 'cite' },
	{ name: 'Inline Quotation',	element: 'q' },

    //{ name: 'Language: RTL',	element: 'span', attributes: { 'dir': 'rtl' } },
    //{ name: 'Language: LTR',	element: 'span', attributes: { 'dir': 'ltr' } },

	/* Object Styles */
    // Applied to special selectable objects (not textual), whenever such
    // selection is supported by the browser. These apply to the following
    // elements: a, embed, hr, img, li, object, ol, table, td, tr, and ul.

    //{
    //    name: 'Styled image (left)',
    //    element: 'img',
    //    attributes: { 'class': 'left' }
    //},

    //{
    //    name: 'Styled image (right)',
    //    element: 'img',
    //    attributes: { 'class': 'right' }
    //},

	{
		name: 'Compact table',
		element: 'table',
		attributes: {
			cellpadding: '5',
			cellspacing: '0',
			border: '1',
			bordercolor: '#ccc'
		},
		styles: {
			'border-collapse': 'collapse'
		}
	},

	//{ name: 'Borderless Table',		element: 'table',	styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
	//{ name: 'Square Bulleted List',	element: 'ul',		styles: { 'list-style-type': 'square' } },


	/* Custom Styles */
    // Plugins may define special style handlers which can be applied in special
    // situations. One of such custom handlers is defined for widgets.
    //{ name: 'Code snippet', type: 'widget', widget: 'codeSnippet', attributes: { 'class': 'pulledSnippet narrow' } },

]);

