from django.conf.urls import *  # NOQA
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from cms.sitemaps import CMSSitemap

admin.autodiscover()

urlpatterns = i18n_patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^', include('cms.urls')),
)

### Google Search
if 'googlesearch' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', (r'^search/', include('googlesearch.urls')), )

### Settings
if 'livesettings' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^settings/', include('livesettings.urls')), )

### Sitemap URLS
if 'django.contrib.sitemaps' in settings.INSTALLED_APPS:
    from cms.sitemaps import CMSSitemap

    urlpatterns += patterns('', url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap',
                                    {'sitemaps': {'cmspages': CMSSitemap}}), )

### Django-Facebook
if 'django_facebook' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', (r'^facebook/', include('django_facebook.urls')), )
    if not 'registration' in settings.INSTALLED_APPS:
        #Don't add this line if you use django registration or userena for registration and auth.
        urlpatterns += patterns('', (r'^accounts/', include('django_facebook.auth_urls')), )

### Registration URLS if it is being used
if 'registration' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^accounts/', include('registration.backends.default.urls')), )

    ### Also add in dynamic urls for Social Auth if it is being used
    if 'social.apps.django_app.default' in settings.INSTALLED_APPS:
        urlpatterns += patterns('', url('', include('social.apps.django_app.urls', namespace='social')), )

### Tiny MCE - a better editor
if 'tinymce' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', (r'^tinymce/', include('tinymce.urls')), )

### Zinnia
if 'zinnia' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^weblog/', include('zinnia.urls')),
        url(r'^comments/', include('django.contrib.comments.urls')),
    )

### django-simple-captcha
if 'captcha' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^captcha/', include('captcha.urls')), )

### Store Locator
if 'store_locator' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^store-locator/', include('store_locator.urls')), )

### Forms Builder
if 'forms_builder.forms' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^forms/', include('forms_builder.forms.urls')), )

### HelpDesk
if 'helpdesk' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', (r'helpdesk/', include('helpdesk.urls')), )

### Stacks
if 'django_select2' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^select2/', include('django_select2.urls')), )


########################################
### For TESTING ONLY - This is only needed when using runserver.
if settings.DEBUG:
    ### Static media
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ) + staticfiles_urlpatterns() + urlpatterns  # NOQA

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns += patterns('',
            url(r'^__debug__/', include(debug_toolbar.urls)),
        )

        ### Django Debug Toolbar module - HTMLTidy
        if 'debug_toolbar_htmltidy' in settings.INSTALLED_APPS:
            urlpatterns += patterns('',
                url(r'^', include('debug_toolbar_htmltidy.urls'))
            )

        ### Required login
        #from django.contrib.auth.decorators import login_required
        #urlpatterns += patterns('',
        #    (r'^securearea/(.*)', login_required(myapp.vew)),
        #)


