#!/usr/bin/env bash

APTPROXY="/etc/apt/apt.conf.d/02proxy"

HOMEDIR="/home/ubuntu/"
PYTHONDIR="${HOMEDIR}python/"
PIPCACHEDIR="${HOMEDIR}pip-cache/"
DJANGODIR=${HOMEDIR}django/
SOUPMIX="${DJANGODIR}soupmix/"
REQUIREMENTS="${SOUPMIX}common_requirements.txt"

###DEBUG: this will not work outside my system!!!
# Add my local proxy to speed up instalation and updates
if [ ! -d ${APTPROXY} ]; then
cat << EOF > ${APTPROXY}
Acquire::http { Proxy "http://192.168.1.2:3142"; };
EOF
fi

# Upgrade the system to current "safe" distribution level
apt-get update -qqy
apt-get upgrade -qqy

# Get the basics
apt-get install -qqy \
    build-essential \
    python-setuptools \
    python-dev \
    python-virtualenv \
    git-core \
    mercurial \
    python-pip \
    libtidy-dev \
    libfreetype6-dev \
    libjpeg62 \
    libjpeg62-dev \
    libjpeg8 \
    libxml2-dev \
    libxslt1-dev \
    node-less \
    uwsgi \
    uwsgi-plugin-python \
    nginx \
    nginx-common \
    nginx-full \
    sqlite3 \
    uwsgi-core \
    uwsgi-plugins-all \
    uwsgi-extra \
    gcc

# Ubuntu x64 - for Python Imaging Library:
if [ ! -f /usr/lib/libjpeg.so ]; then
    ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so /usr/lib/
fi
if [ ! -f /usr/lib/libfreetype.so ]; then
    ln -s /usr/lib/x86_64-linux-gnu/libfreetype.so /usr/lib/
fi
if [ ! -f /usr/lib/libz.so ]; then
    ln -s /usr/lib/x86_64-linux-gnu/libz.so /usr/lib/
fi
apt-get install -qqy python-imaging


###############################################################################

# silently fail if it exists, but ensure the full path exists
mkdir -p ${PIPCACHEDIR}

# Get or update the soupmix (CMS3 branch)
if [ -d ${SOUPMIX} ]; then
    cd ${SOUPMIX}
    git pull
else
    git clone https://git@bitbucket.org/oddotterco/soupmix.git --branch CMS3 --single-branch ${SOUPMIX}
fi

# Create the PVE and install the mix if needed
if [ ! -d ${PYTHONDIR} ]; then
    echo 'Creating PVE and installing modules, please wait (may take a few minutes)...'
    virtualenv --distribute --no-site-packages ${PYTHONDIR}
    source ${PYTHONDIR}bin/activate
    pip install -q --download-cache=${PIPCACHEDIR} --exists-action=i -I -r ${REQUIREMENTS}
fi

# Also, to allow logging in as user "ubuntu", copy autorized_keys file
cp ~vagrant/.ssh/authorized_keys ~ubuntu/.ssh/

chown -R ubuntu:ubuntu ~ubuntu

