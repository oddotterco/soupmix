
##############################################################################
#### EXTRA SOUP MIXINS ---------------------------------------------------####
##############################################################################

import os
from django.utils.translation import ugettext_lazy as _
gettext = lambda s: s

# Enable plugin and content caching
# Don't forget to: ./manage.py createcachetable cache_data
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'cache_data',
    },
}

# Application definition
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(os.path.dirname(os.path.dirname(__file__)), 'data.db'),
        'HOST': 'localhost',
        'USER': '',
        'PASSWORD': '',
        'PORT': '',
    }
}

INSTALLED_APPS += (
    'django.contrib.admindocs',
    'djangocms_snippet',
)


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

CMS_TEMPLATES = (
    ## Customize this
    ('base.html', 'Page'),
    ('page.html', 'Page with sidebar'),
    ('feature.html', 'Page with Feature'),
)


# SEE: https://developer.mozilla.org/en-US/docs/HTTP/X-Frame-Options
#X_FRAME_OPTIONS = "ALLOW-FROM https://apps.facebook.com/"
# This is no longer needed in CMS 3.2+ as it can be enabled on a per-page basis

#### Django Reversion https://code.google.com/p/django-reversion/wiki/GettingStarted
CMS_MAX_PAGE_PUBLISH_REVERSIONS = 5

# To solve some odd issues with newer image types:
import mimetypes

mimetypes.add_type("image/svg+xml", ".svg", True)
mimetypes.add_type("image/svg+xml", ".svgz", True)
mimetypes.add_type("image/png", ".png", True)

#outgoing mail server settings
SERVER_EMAIL = 'youremail@yourdomain.tld'
DEFAULT_FROM_EMAIL = 'youremail@yourdomain.tld'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'youremail@yourdomain.tld'
EMAIL_HOST_PASSWORD = 'CHANGE_THIS'
EMAIL_SUBJECT_PREFIX = ''
EMAIL_PORT = '587'
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

#######################################
#### Django-Filer CMS-Plugins - improved files and graphics handling
#### http://readthedocs.org/docs/django-filer/en/latest/
#THUMBNAIL_DEBUG = DEBUG
#THUMBNAIL_QUALITY = 85
#FILER_IS_PUBLIC_DEFAULT = True

#######################################
### DjangoCMS Column plugin
COLUMN_WIDTH_CHOICES = (
    ('col-md-1', _("1/12")),
    ('col-md-2', _("2/12")),
    ('col-md-3', _("3/12")),
    ('col-md-4', _("4/12")),
    ('col-md-5', _("5/12")),
    ('col-md-6', _("6/12")),
    ('col-md-7', _("7/12")),
    ('col-md-8', _("8/12")),
    ('col-md-9', _("9/12")),
    ('col-md-10', _("10/12")),
    ('col-md-11', _("11/12")),
    ('col-md-12', _("12/12")),
    ('col-full', _("full-width")),
)
# For additional support for BootStrap in the editor:
CKEDITOR_SETTINGS = {
    'language': '{{ language }}',
    'toolbar_HTMLField': [
        ['Undo', 'Redo'],
        ['ShowBlocks'],
        ['Format', 'Styles'],
    ],
    'skin': 'moono',
    'stylesSet': 'default:/static/js/ckeditor_bootstrap.js',
}
TEXT_ADDITIONAL_TAGS = ('scrolling', 'allowfullscreen', 'frameborder')

#######################################
### Extra contrib items
# Good lorem tools for development, etc
INSTALLED_APPS += ('django.contrib.webdesign',)
INSTALLED_APPS += ('django.contrib.humanize',)

#######################################
#### Aldryn Blog
#### https://github.com/aldryn/aldryn-blog
INSTALLED_APPS += (
    'aldryn_blog',
    'aldryn_common',
    'django_select2',
    'taggit',
    'hvad',
    # for search
    'aldryn_search',
    'haystack',
)
SELECT2_BOOTSTRAP = True
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8983/solr',
        'TIMEOUT': 60 * 5,
        'INCLUDE_SPELLING': True,
        'BATCH_SIZE': 100,
    },
}
ALDRYN_SEARCH_REGISTER_APPHOOK = True
ALDRYN_BLOG_SEARCH = True


#######################################
#### Django Compressor configuration
INSTALLED_APPS += ('compressor',)
if 'compressor' in INSTALLED_APPS:
    COMPRESS_ENABLED = True
    if not 'compressor.finders.CompressorFinder' in STATICFILES_FINDERS:
        STATICFILES_FINDERS += (
            'compressor.finders.CompressorFinder',
        )
    COMPRESS_PRECOMPILERS = (
        ('text/less', 'lessc {infile} {outfile}'),
    )

    COMPRESS_URL = STATIC_URL
    COMPRESS_ROOT = STATIC_ROOT
    COMPRESS_OUTPUT_DIR = 'lesscache'
    COMPRESS_CSS_FILTERS = (
        'compressor.filters.template.TemplateFilter',
        'compressor.filters.cssmin.CSSMinFilter',
        'compressor.filters.css_default.CssAbsoluteFilter',
    )
    COMPRESS_TEMPLATE_FILTER_CONTEXT = {
        "WARNING_COLOR": "red",
    }


#######################################
#### Django DebugToolbar
#### https://github.com/django-debug-toolbar/django-debug-toolbar
INTERNAL_IPS = ('127.0.0.1', )
INSTALLED_APPS += ('debug_toolbar',)
DEBUG_TOOLBAR_PATCH_SETTINGS = False
MIDDLEWARE_CLASSES = (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
) + MIDDLEWARE_CLASSES
DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
)
#INSTALLED_APPS += (
#    'debug_toolbar_htmltidy',
#)


#######################################
#### Django Contact PLUS!
#### https://github.com/arteria/cmsplugin-contact-plus/
INSTALLED_APPS += ('cmsplugin_contact_plus',)
# Do not forget to add 'django.template.loaders.app_directories.Loader' to TEMPLATE_LOADERS


#######################################
#### Django-Registration
#### https://bitbucket.org/ubernostrum/django-registration
INSTALLED_APPS += ('registration',)
if 'registration' in INSTALLED_APPS:
    LOGIN_REDIRECT_URL = '/'
    ACCOUNT_ACTIVATION_DAYS = 30


#######################################
#### Python-Social-Auth
#### http://python-social-auth.readthedocs.org/en/latest/configuration/django.html
INSTALLED_APPS += ('social.apps.django_app.default',)
if 'social.apps.django_app.default' in INSTALLED_APPS:
    TEMPLATE_CONTEXT_PROCESSORS += (
        'social.apps.django_app.context_processors.backends',
        'social.apps.django_app.context_processors.login_redirect',
    )
    ### Enable as many of these social networks as desired
    ### Make sure to also specify the API keys for each
    AUTHENTICATION_BACKENDS = (
        #'social.backends.amazon.AmazonOAuth2',
        #'social.backends.angel.AngelOAuth2',
        #'social.backends.aol.AOLOpenId',
        #'social.backends.appsfuel.AppsfuelOAuth2',
        #'social.backends.behance.BehanceOAuth2',
        #'social.backends.belgiumeid.BelgiumEIDOpenId',
        #'social.backends.bitbucket.BitbucketOAuth',
        #'social.backends.box.BoxOAuth2',
        #'social.backends.coinbase.CoinbaseOAuth2',
        #'social.backends.dailymotion.DailymotionOAuth2',
        #'social.backends.disqus.DisqusOAuth2',
        #'social.backends.douban.DoubanOAuth2',
        #'social.backends.dropbox.DropboxOAuth',
        #'social.backends.evernote.EvernoteSandboxOAuth',
        'social.backends.facebook.FacebookAppOAuth2',
        'social.backends.facebook.FacebookOAuth2',
        #'social.backends.fedora.FedoraOpenId',
        #'social.backends.fitbit.FitbitOAuth',
        #'social.backends.flickr.FlickrOAuth',
        #'social.backends.foursquare.FoursquareOAuth2',
        #'social.backends.github.GithubOAuth2',
        'social.backends.google.GoogleOAuth',
        'social.backends.google.GoogleOAuth2',
        'social.backends.google.GoogleOpenId',
        'social.backends.google.GooglePlusAuth',
        #'social.backends.instagram.InstagramOAuth2',
        #'social.backends.jawbone.JawboneOAuth2',
        #'social.backends.linkedin.LinkedinOAuth',
        #'social.backends.linkedin.LinkedinOAuth2',
        #'social.backends.live.LiveOAuth2',
        #'social.backends.livejournal.LiveJournalOpenId',
        #'social.backends.mailru.MailruOAuth2',
        #'social.backends.mendeley.MendeleyOAuth',
        #'social.backends.mixcloud.MixcloudOAuth2',
        #'social.backends.odnoklassniki.OdnoklassnikiOAuth2',
        #'social.backends.open_id.OpenIdAuth',
        #'social.backends.openstreetmap.OpenStreetMapOAuth',
        #'social.backends.orkut.OrkutOAuth',
        #'social.backends.persona.PersonaAuth',
        #'social.backends.podio.PodioOAuth2',
        #'social.backends.rdio.RdioOAuth1',
        #'social.backends.rdio.RdioOAuth2',
        #'social.backends.readability.ReadabilityOAuth',
        #'social.backends.reddit.RedditOAuth2',
        #'social.backends.runkeeper.RunKeeperOAuth2',
        #'social.backends.skyrock.SkyrockOAuth',
        #'social.backends.soundcloud.SoundcloudOAuth2',
        #'social.backends.stackoverflow.StackoverflowOAuth2',
        #'social.backends.steam.SteamOpenId',
        #'social.backends.stocktwits.StocktwitsOAuth2',
        #'social.backends.stripe.StripeOAuth2',
        #'social.backends.suse.OpenSUSEOpenId',
        #'social.backends.thisismyjam.ThisIsMyJamOAuth1',
        #'social.backends.trello.TrelloOAuth',
        #'social.backends.tripit.TripItOAuth',
        #'social.backends.tumblr.TumblrOAuth',
        #'social.backends.twilio.TwilioAuth',
        'social.backends.twitter.TwitterOAuth',
        #'social.backends.vk.VKOAuth2',
        #'social.backends.weibo.WeiboOAuth2',
        #'social.backends.xing.XingOAuth',
        #'social.backends.yahoo.YahooOAuth',
        #'social.backends.yahoo.YahooOpenId',
        #'social.backends.yammer.YammerOAuth2',
        #'social.backends.yandex.YandexOAuth2',
        'social.backends.email.EmailAuth',
        'social.backends.username.UsernameAuth',
        'django.contrib.auth.backends.ModelBackend',
    )

    # http://psa.matiasaguirre.net/docs/backends/

    # Login URLs - these must reflect, accurately, the project's settings
    # Django Auth Settings
    LOGIN_URL = "/accounts/login"
    LOGIN_REDIRECT_URL = '/'
    URL_PATH = ''

    SOCIAL_AUTH_STRATEGY = 'social.strategies.django_strategy.DjangoStrategy'
    SOCIAL_AUTH_STORAGE = 'social.apps.django_app.default.models.DjangoStorage'

    # To enable your app to use a social authentication system, look at
    # http://psa.matiasaguirre.net/docs/backends/twitter.html
    SOCIAL_AUTH_GOOGLE_OAUTH_SCOPE = [
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/userinfo.profile'
    ]

    # Facebook AUTH - http://developers.facebook.com/docs/reference/api/permissions/
    SOCIAL_AUTH_FACEBOOK_KEY = ''
    SOCIAL_AUTH_FACEBOOK_SECRET = ''
    SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_likes', 'public_profile', 'user_birthday']

    # Facebook APP - https://developers.facebook.com/apps/
    SOCIAL_AUTH_FACEBOOK_APP_KEY = ''
    SOCIAL_AUTH_FACEBOOK_APP_SECRET = ''
    SOCIAL_AUTH_FACEBOOK_APP_NAMESPACE = '{{ project_name }}_app'

    # Twitter - https://dev.twitter.com/apps/
    SOCIAL_AUTH_TWITTER_KEY = ''
    SOCIAL_AUTH_TWITTER_SECRET = ''

    # https://linkedin.com/secure/developer

#######################################
#### Store Locator
#### https://github.com/MegaMark16/django-cms-storelocator
#INSTALLED_APPS += ('store_locator',)

#######################################
#### Live Settings
#### http://django-livesettings.readthedocs.org/en/latest/
INSTALLED_APPS += ('livesettings',)


#######################################
#### DjangoCMS Minecraft
#### https://bitbucket.org/oddotterco/djangocms-minecraft
#INSTALLED_APPS += ('djangocms_minecraft',)

#######################################
#### Nivo Slider
#### https://bitbucket.org/bercab/cmsplugin-nivoslider
#INSTALLED_APPS += ('cmsplugin_nivoslider', )
